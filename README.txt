README
------

This module is based on drupal.org/project/guzzle but has been modified to
include the composer build for easier deployment under Aegir.

This module doesn't actually do anything, but by enabling this module,
composer_autoload will detect the guzzle libraries and include them.
